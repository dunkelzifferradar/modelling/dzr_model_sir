from dotenv import load_dotenv
from dzr_shared_util.db.db_conn import DBConnector
from matplotlib import pyplot
from pandas import DataFrame

from consts import ROOT_DIR
from model.model_siru import ModelSIRU
from model.siru_runner import _get_batch_data_basis

load_dotenv(ROOT_DIR + "/.env")


def plot_siru(df_input, df_res):
    ax = pyplot.gca()
    # df_input = df_input[df_res["REPORT_DATE"] < datetime.strptime("2020-03-20", "%Y-%m-%d")]
    # df_res = df_res[df_res["REPORT_DATE"] < datetime.strptime("2020-03-20", "%Y-%m-%d")]
    df_res.plot(kind='line', x='REPORT_DATE', y='REPORTED_ACTIVE_CASES', ax=ax)
    df_res.plot(kind='line', x='REPORT_DATE', y='NEW_REPORTED', color='orange', ax=ax)
    df_input.plot(kind='line', x='REPORT_DATE', y='CASE_COUNT', color='black', ax=ax)
    df_res["TOTAL"] = df_res["REPORTED_ACTIVE_CASES"] \
                      + df_res[ModelSIRU.COL_ACTIVE_INFECTIONS_ASYMPTOMATIC] \
                      + df_res[ModelSIRU.COL_ACTIVE_UNREPORTED]
    #df_res.plot(kind='line', x='REPORT_DATE', y='TOTAL', color='orange', ax=ax)
    #df_res.plot(kind='line', x='REPORT_DATE', y='UNREPORTED_ACTIVE_CASES', color='brown', ax=ax)
    df_res.plot(kind='line', x='REPORT_DATE', y=ModelSIRU.COL_ACTIVE_INFECTIONS_ASYMPTOMATIC, color='red', ax=ax)
    df_input["ICR_R"] = 100 / df_input["ICR_EXP_VAL"]
    df_res["ICR_COL_SMOOTH_R"] = 100 / df_res[ModelSIRU.COL_ICR_SMOOTH]
    df_input.plot(kind='line', x='REPORT_DATE', y='ICR_R', color='blue', ax=ax)
    df_res.plot(kind='line', x='REPORT_DATE', y='ICR_COL_SMOOTH_R', color='pink', ax=ax)
    df_res.plot(kind='line', x='REPORT_DATE', y='NEW_INFECTED', color='green', ax=ax)
    pyplot.show()
    ax = pyplot.axes()
    df_res["REPORTED_CASE_SUM"] = df_res["NEW_REPORTED"].shift(-7).ffill().cumsum()
    df_res["TOTAL_CASE_SUM"] = df_res["NEW_INFECTED"].cumsum()
    # todo: da die Neuinfektionen mit diesem "Padding" auf min 0 gebracht werden aber die reported cases steigen,
    #  kommt es hier noch zu rückläufiger Entwicklung. Das ist unschön und zu überlegen.
    df_res["UNREPORTED_CASE_SUM"] = df_res["TOTAL_CASE_SUM"] - df_res["REPORTED_CASE_SUM"]
    df_res.plot(kind='line', x='REPORT_DATE', y='REPORTED_CASE_SUM', ax=ax)
    df_res.plot(kind='line', x='REPORT_DATE', y='UNREPORTED_CASE_SUM', color='red', ax=ax)
    df_res.plot(kind='line', x='REPORT_DATE', y='TOTAL_CASE_SUM', color='green', ax=ax)
    pyplot.show()


db = DBConnector()
# target_id = 11000  # Berlin
# target_id = 9178  # Freising
# target_id = 9187  # Rosenheim
# target_id = 3103  # Wolfsburg
target_id = 8111  # Stuttgart
# target_id = 8115 # Böblingen
# target_id = 9161 # Ingolstadt
# target_id = 5370 # Heinsberg
# target_id = 5754 # Gütersloh
# target_id = 3159 # Göttingen
# target_id = 5  # NRW
# target_id = 9  # Bayern
# target_id = 8 # BaWü
# target_id = 0 # D
icr_params = {
    "DAYS_AGGR": 7,
    "GEO_AGGR": True,
    "IFR_BASE": 0.00657
}
try:
    df_input, population = _get_batch_data_basis(db, [target_id], "COUNTY", icr_params)
    # df_input, population = get_batch_data_basis(db, [target_id], "STATE", icr_params)
    # df_input.to_csv("./df_input.csv", index=False)
    # print(population)
    # df_input = read_csv("df_input.csv", sep=',', parse_dates=["REPORT_DATE"])
    # population = {5: {'POPULATION': 17932651}}
    #df_input, population = get_batch_data_basis(db, [target_id], "COUNTRY", icr_params)
finally:
    #pass
    db.close_connection()

siru = ModelSIRU(days_infect_asymp=7, days_infect_symp=7)
df_res: DataFrame = siru.evaluate(df_input, population[target_id]["POPULATION"])
plot_siru(df_input, df_res)
