import logging
import os
from datetime import datetime

"""File system info"""
ROOT_DIR = os.path.dirname(os.path.abspath(__file__))

"""DB Table names"""
CASE_TABLE = "SOR_FACT_CASE"
ICR_TBL_NAME = "NA"
PARAM_TBL_STATE = 'DM_MDL_PRM_STATE'
PARAM_TBL_COUNTY = 'DM_MDL_PRM_COUNTY'
PARAM_TBL_COUNTRY = 'DM_MDL_PRM_COUNTRY'
DIM_COUNTY_TBL = 'SOR_DIM_COUNTY'
DIM_STATE_TBL = 'SOR_DIM_STATE'
DIM_COUNTRY_TBL = 'SOR_DIM_COUNTRY'
ICR_TBL_COUNTY = 'SOR_MDL_ICR_COUNTY'
ICR_TBL_STATE = 'SOR_MDL_ICR_STATE'
ICR_TBL_COUNTRY = 'SOR_MDL_ICR_COUNTRY'
"""Data constants"""
DEFAULT_START_DATE = datetime.strptime("2020-03-09", "%Y-%m-%d")
"""Logging"""
logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger(__name__)
