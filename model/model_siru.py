import logging

import numpy as np
from pandas import DataFrame

logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger(__name__)

row_cnt = 0


class ModelSIRU:
    """
    Epidemiological model SIRU.


    The class is stateless and contains only model logic. All required data must be given as input. It is therefore
    agnostic of the geography or time frame of a modeling target.
    """
    COL_DATE = "REPORT_DATE"
    COL_ACTIVE_UNREPORTED = "UNREPORTED_ACTIVE_CASES"
    COL_ACTIVE_INFECTIONS_ASYMPTOMATIC = "ACTIVE_INFECTIONS_ASYMPTOMATIC"
    COL_ACTIVE_REPORTED = "REPORTED_ACTIVE_CASES"
    COL_ICR_SMOOTH = "ICR_SMOOTHED"
    COL_ICR_RAW = "ICR_EXP_VAL"
    COL_CASE_SUM = "CASE_SUM"
    COL_CASE_COUNT = "CASE_COUNT"
    COL_NEW_REPORTED = "NEW_REPORTED"
    COL_NEW_UNREPORTED = "NEW_UNREPORTED"
    COL_NEW_INFECTED = "NEW_INFECTED"

    def __init__(self, days_infect_symp, days_infect_asymp) -> None:
        super().__init__()
        self.days_infected_symptomatic = days_infect_symp
        self.days_infected_asymptomatic = days_infect_asymp
        self.nu = 1 / self.days_infected_asymptomatic
        self.eta = 1 / self.days_infected_symptomatic

    def evaluate(self, df_timeseries: DataFrame, population: int) -> DataFrame:
        """
        Models the course of an epidemic for the given input.

        More precisely, computes the active reported cases, active infections and active unreported
        cases over the timeframe provided in the timeseries input parameter.

        :param df_timeseries: The data (reported cases, ICR values) per day. Is expected to be for
            one target only - meaning each day is only contained once in this timeseries.
        :param population: total population count
        :return: DataFrame with all modelled aspects of the epidemic over the time frame given in the input.
        """
        exp_cols = {self.COL_CASE_COUNT, self.COL_DATE, self.COL_ICR_RAW, self.COL_CASE_SUM}
        assert all([col in df_timeseries.columns for col in exp_cols]), "Missing columns in " \
                                                                        + str(df_timeseries.columns)
        assert not df_timeseries.empty, "Empty DataFrame provided."
        # smoothen ICR
        lag = 10

        def sample_icr(val):
            ret = np.nan
            global row_cnt
            if row_cnt % 7 == 0:
                ret = val
            row_cnt += 1
            return ret

        sampled_icr = df_timeseries[self.COL_ICR_RAW].apply(sample_icr)
        df_timeseries[self.COL_ICR_SMOOTH] = sampled_icr.interpolate().ewm(span=lag).mean()\
            .ffill().bfill()
        # compute active reported cases
        df_r = self._compute_reported_active(df_timeseries)
        df_r_i = self._compute_infectious_asymptomatic(df_r)
        # if splitting into multiple timeframes, a start value for u can be set here instead of None
        df_r_i_u = self._compute_unreported_active(df_r_i, u_0=None)
        # todo: use population to cap off sum (I + U + R) for every day
        df_res = self._compute_new_infected(df_r_i_u)
        return df_res[['ACTIVE_INFECTIONS_ASYMPTOMATIC',
                       'NEW_INFECTED',
                       'NEW_REMOVED_R',
                       'NEW_REPORTED',
                       'REPORTED_ACTIVE_CASES',
                       'REPORT_DATE',
                       'UNREPORTED_ACTIVE_CASES',
                       'ICR_SMOOTHED']]

    def _compute_reported_active(self, df_timeseries: DataFrame) -> DataFrame:
        # computes the active reported cases based on the "new reported cases" given in the input per day.
        new_reported = self._smoothen_new_reported(df_timeseries)
        reported_active = [0] * len(new_reported)
        new_removed_r = [0] * len(new_reported)
        for t in range(0, len(reported_active)):
            # take the sum of newly reported cases over the past x days - where x is the avg time symptomatic
            #  infectious have symptoms
            start_active_timeframe = max(0, t - self.days_infected_symptomatic + 1)
            reported_active[t] = sum(new_reported[start_active_timeframe:t + 1])
            # also compute the removed ones, i.e. those reported cases that are becoming inactive as illness is over
            if start_active_timeframe == 0:
                new_removed_r[t] = 0
            else:
                # take the new reports from the day "today minus (<average time of illness> + 1)", as those now
                #  (statistically) become inactive
                new_removed_r[t] = new_reported[start_active_timeframe - 1]
        df_new_reported = df_timeseries[[self.COL_DATE, self.COL_ICR_SMOOTH]].copy()
        df_new_reported[self.COL_NEW_REPORTED] = new_reported
        df_new_reported[self.COL_ACTIVE_REPORTED] = reported_active
        # fill last entry of removed reported with
        df_new_reported["NEW_REMOVED_R"] = new_removed_r
        return df_new_reported

    def _smoothen_new_reported(self, df_timeseries) -> list:
        # resample and aggr so that all cases in a week are spread evenly across that week
        days_aggr = 7
        # compute sums per week
        df_resampled_days_aggr = df_timeseries[[self.COL_DATE, self.COL_CASE_COUNT]] \
            .resample(str(days_aggr) + 'D', on=self.COL_DATE).sum().reset_index().copy()
        # join sum-series (per week) with all dates
        df_resampled_days = df_timeseries[[self.COL_DATE]].set_index(self.COL_DATE) \
            .join(df_resampled_days_aggr.set_index(self.COL_DATE), how='left')
        # correct last entry if necessary: if number of all days is not divisible by 7, the last number will be
        # much smaller
        #   get last entry
        idx_last_entry = max(df_resampled_days[df_resampled_days[self.COL_CASE_COUNT].isna() == False].index)
        #   normalize (virtually spread across same number of days as the others)
        corrected_last_val = df_resampled_days[self.COL_CASE_COUNT][idx_last_entry] \
                             * days_aggr \
                             / ((max(df_resampled_days.index) - idx_last_entry).days + 1)
        df_resampled_days[self.COL_CASE_COUNT][idx_last_entry] = corrected_last_val
        # interpolate to fill nas
        df_resampled_days_interpolated = df_resampled_days.interpolate()
        df_resampled_days[self.COL_CASE_SUM] = df_resampled_days_interpolated[self.COL_CASE_COUNT].apply(
            lambda x: x / float(days_aggr)).cumsum()
        # smoothen over sum curve
        smooth_case_sum = df_resampled_days[self.COL_CASE_SUM].ewm(span=5).mean() \
            .ffill().bfill() \
            .to_list()
        smooth_new_reported = [0] * len(smooth_case_sum)
        smooth_new_reported[0] = smooth_case_sum[0]
        for t in range(1, len(smooth_case_sum)):
            smooth_new_reported[t] = smooth_case_sum[t] - smooth_case_sum[t - 1]
        return smooth_new_reported

    def _compute_infectious_asymptomatic(self, df: DataFrame) -> DataFrame:
        # computes the active infectious population based on the "active reported cases" per day computed before.
        reported_active_ts = df[self.COL_ACTIVE_REPORTED].to_list()
        f_ts = df[self.COL_ICR_SMOOTH].to_list()
        asymp_infectious_ts = [0] * len(reported_active_ts)
        for t in range(1, len(reported_active_ts)):
            asymp_infectious_ts[t - 1] = max((reported_active_ts[t] + (self.eta - 1) * reported_active_ts[t - 1]) \
                                             / (f_ts[t - 1] * self.nu), 0)
        # for the final value, the above formula would require tomorrow's active reported cases. As these are
        # naturally not known yet, we approximate it by using last day's value.
        t_n = len(asymp_infectious_ts) - 1
        # compute reported active for t_n+1
        asymp_infectious_ts[t_n] = asymp_infectious_ts[t_n - 1]
        df[self.COL_ACTIVE_INFECTIONS_ASYMPTOMATIC] = asymp_infectious_ts
        return df

    def _compute_unreported_active(self, df: DataFrame, u_0: int = None) -> DataFrame:
        # computes the active unreported cases based on the "active infectious" per day computed before.
        r_ts = df[self.COL_ACTIVE_REPORTED].to_list()
        i_ts = df[self.COL_ACTIVE_INFECTIONS_ASYMPTOMATIC].to_list()
        f_ts = df[self.COL_ICR_SMOOTH].to_list()
        assert len(f_ts) >= len(r_ts) - 1, "Error: list of ICR values too short"
        unreported_active_ts = [0] * len(r_ts)
        new_unreported = [0] * len(r_ts)
        # if start value is given, use it, else use # Reported / ICR - Reported
        if u_0 is not None:
            unreported_active_ts[0] = u_0
        else:
            unreported_active_ts[0] = (r_ts[0] / f_ts[0]) - r_ts[0]
        for t in range(1, len(r_ts)):
            unreported_active_ts[t] = max((1 - self.eta) * unreported_active_ts[t - 1] \
                                          + (1 - f_ts[t - 1]) * self.nu * i_ts[t - 1], 0)
            new_unreported[t] = (1 - f_ts[t - 1]) * self.nu * i_ts[t - 1]
        df[self.COL_ACTIVE_UNREPORTED] = unreported_active_ts
        df[self.COL_NEW_UNREPORTED] = new_unreported
        return df

    def _compute_new_infected(self, df_r_i_u: DataFrame) -> DataFrame:
        # computes the number of new infectious individuals based on the "active infectious" per day computed before.
        i_ts = df_r_i_u[self.COL_ACTIVE_INFECTIONS_ASYMPTOMATIC].to_list()
        r_new_ts = df_r_i_u[self.COL_NEW_REPORTED].to_list()
        u_new_ts = df_r_i_u[self.COL_NEW_UNREPORTED].to_list()
        i_new_ts = [0] * len(i_ts)
        i_new_ts[0] = i_ts[0]
        negative_padding = 0
        for t in range(1, len(i_ts)):
            val = i_ts[t] - i_ts[t - 1] + r_new_ts[t] + u_new_ts[t]
            if val < 0:
                negative_padding += val
                i_new_ts[t] = 0
            else:
                adjusted_val = max(val + negative_padding, 0)
                i_new_ts[t] = adjusted_val
                negative_padding = min(negative_padding + val, 0)
        df_res = df_r_i_u.copy()
        df_res[self.COL_NEW_INFECTED] = i_new_ts
        return df_res
