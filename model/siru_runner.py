"""
Handles requests to run the SIRU model, procures the required data and prepares the request response.
"""
from dzr_shared_util.db.db_conn import DBConnector
from pandas import DataFrame, Series

from consts import PARAM_TBL_STATE, PARAM_TBL_COUNTY, PARAM_TBL_COUNTRY, DIM_STATE_TBL, DIM_COUNTY_TBL, \
    DIM_COUNTRY_TBL, ICR_TBL_STATE, ICR_TBL_COUNTY, ICR_TBL_COUNTRY, logger
from model.model_siru import ModelSIRU

EXP_INPUT_FIELDS = {"TARGET_ID", "DAYS_INFECT_SYMP", "DAYS_INFECT_ASYMP", "TARGET_LEVEL", "ICR_PARAMS"}
EXP_ICR_PARAM_FIELDS = {"IFR_BASE", "DAYS_AGGR", "GEO_AGGR"}
MODEL_RESULT_FIELDS = {ModelSIRU.COL_DATE,
                       ModelSIRU.COL_NEW_REPORTED,
                       ModelSIRU.COL_ACTIVE_REPORTED,
                       ModelSIRU.COL_NEW_INFECTED,
                       ModelSIRU.COL_ACTIVE_INFECTIONS_ASYMPTOMATIC,
                       ModelSIRU.COL_ACTIVE_UNREPORTED}
RUNNER_RETURN_FIELDS = {"DATE",
                        "TARGET_ID",
                        "NEW_REPORTED",
                        "ACTIVE_REPORTED",
                        "SUM_REPORTED",
                        "NEW_INFECTIONS",
                        "ACTIVE_INFECTIONS",
                        "SUM_INFECTIONS"}


def _create_empty_result_dict():
    # Creates an empty dictionary in the shape of a SIRU model run's result.
    dict = {}
    for col in RUNNER_RETURN_FIELDS:
        dict[col] = []
    return dict


def _prep_sql_in_subquery(lst: list):
    # SQL-stringifies a list of elements
    return "(" + ", ".join([str(elem) for elem in lst]) + ")"


def _fill_reported_on_final_days(df: DataFrame, target_ids: list) -> DataFrame:
    # The SIRU model requires reported case data for the whole timeframe for which it shall model the pandemic.
    # When case data for the most recent day(s) is not available yet, this method makes sure to fill these days
    # by computing the average reported case value for the week leading up to the last reported date.
    df.reset_index(inplace=True)
    all_rep_cases = Series([])
    for target_id in target_ids:
        rep_cases = df[df['TARGET_ID'] == target_id]['CASE_COUNT'].copy()
        # get index of last day for which reported case data is available
        idx_final_with_rep_cases = max(rep_cases[rep_cases.isna() == False].index)
        if idx_final_with_rep_cases == max(rep_cases.index):
            return df
        #   for any target id after the first, the indices will start with an offset. To use this type of
        #   "sub-series'ing", the start- and end indices must be starting from 0 however. Hence, subtract offset.
        offset = min(rep_cases.index)
        idx_week_start = max(idx_final_with_rep_cases - 6, min(rep_cases.index)) - offset
        idx_week_end = idx_final_with_rep_cases + 1 - offset
        # Fill nas with 0 to avoid nans in this last week of reported data.
        avg_last_week_float = rep_cases[idx_week_start: idx_week_end] \
            .fillna(0) \
            .aggregate("average")
        avg_last_week = round(avg_last_week_float)
        for idx_unrep_days in range(idx_final_with_rep_cases + 1, max(rep_cases.index)+1):
            rep_cases[idx_unrep_days] = avg_last_week
        all_rep_cases = all_rep_cases.append(rep_cases)
    df["CASE_COUNT"] = all_rep_cases
    return df


def _get_batch_data_basis(db, target_ids, target_level, icr_params: dict):
    # gets the data required for a SIRU model run for all targets: case, ICR and population data.
    assert target_level in ['COUNTY', 'STATE', 'COUNTRY']
    if target_level == 'STATE':
        case_view = PARAM_TBL_STATE
        pop_tbl = DIM_STATE_TBL
        icr_tbl = ICR_TBL_STATE
    elif target_level == "COUNTY":
        case_view = PARAM_TBL_COUNTY
        pop_tbl = DIM_COUNTY_TBL
        icr_tbl = ICR_TBL_COUNTY
    else:
        case_view = PARAM_TBL_COUNTRY
        pop_tbl = DIM_COUNTRY_TBL
        icr_tbl = ICR_TBL_COUNTRY
    target_ids.sort()
    df_cases = db.get_data(case_view, where="TARGET_ID in " + _prep_sql_in_subquery(target_ids))
    if df_cases.empty:
        raise ValueError("No case data found for target id " + str(target_ids))
    # todo: add icr params
    df_icr = db.get_data(icr_tbl, where="TARGET_ID in " + _prep_sql_in_subquery(target_ids)
                                        + " and IFR_BASE = " + str(icr_params["IFR_BASE"])
                                        + " and DAYS_AGGR = " + str(icr_params["DAYS_AGGR"])
                                        + " and GEO_AGGR = " + str(icr_params["GEO_AGGR"]))
    if df_cases.empty:
        raise ValueError("No icr data found for target id " + str(target_ids) + " and ICR params " + str(icr_params))
    # join icr and case dataframes as they both list values per day
    df_cases.set_index(["TARGET_ID", "REPORT_DATE"], inplace=True)
    df_icr.rename(columns={"DATE": "REPORT_DATE"}, inplace=True)
    df_icr.set_index(["TARGET_ID", "REPORT_DATE"], inplace=True)
    df_cases_icr = _fill_reported_on_final_days(df_icr.join(df_cases, how='left').sort_index(), target_ids) \
        .fillna(0)
    # order by date (and before, by target) before cumsumming up
    df_cases_icr.sort_values(by=["TARGET_ID", "REPORT_DATE"], inplace=True)
    # compute cumulative sum of cases.
    df_cases_icr['CASE_SUM'] = df_cases_icr.groupby(["TARGET_ID"],
                                                    as_index=False,
                                                    sort=False)['CASE_COUNT'].cumsum(axis=0)
    # todo: insert front fill like in icr service
    df_ret = df_cases_icr[["TARGET_ID", "REPORT_DATE", "CASE_COUNT", "CASE_SUM", "EXP_VAL"]].rename(columns={
        "EXP_VAL": "ICR_EXP_VAL"
    })
    # grab demographics (for specified target level) from the database
    df_populations = db.get_data(pop_tbl, where="ID in " + _prep_sql_in_subquery(target_ids))
    population_dict = df_populations[["ID", "POPULATION"]].astype(int).set_index("ID").to_dict("index")
    # return cases and icr values, and the populations separately
    return df_ret, population_dict


def run(body: dict):
    """
    Handles a request to run the SIRU model.

    1) Validates the input to contain all required fields.
    2) Pulls the data that the model will require to run for the specified input parameters. For example, the
        case data is pulled from the database for the targets (counties/states/country) specified in the input.
    3) Runs the SIRU model for each target.
    4) Combines all model run results into a response object and returns.

    :param body: a dictionary (e.g. from JSON) containing parameters for SIRU run.
    :return: a tuple of: (result of SIRU model run as a dictionary, HTTP success/error code)
    """
    try:
        logger.info("Starting model run")
        assert set(body.keys()) == EXP_INPUT_FIELDS, \
            "Invalid request body. Expected fields " + str(EXP_INPUT_FIELDS) + ", but got " + str(body.keys())
        logger.debug("Reading input parameters (body)...")
        target_ids = body["TARGET_ID"]
        days_infect_symp = body["DAYS_INFECT_SYMP"]
        days_infect_asymp = body["DAYS_INFECT_ASYMP"]
        target_level = body["TARGET_LEVEL"]
        icr_params = body["ICR_PARAMS"]
        assert set(icr_params.keys()) == EXP_ICR_PARAM_FIELDS, \
            "Invalid request body. Expected fields in object ICR_PARAMS " + str(EXP_ICR_PARAM_FIELDS) \
            + ", but got " + str(icr_params.keys())
        logger.debug("Establishing DB connection...")
        db = DBConnector()
        logger.debug("Getting timeseries data and population for all targets of batch run...")
        try:
            df_daily_data, population_dict = _get_batch_data_basis(db, target_ids, target_level, icr_params)
        except Exception as e:
            logger.exception(e)
            return {"Error": str(e)}, 500
        finally:
            db.close_connection()
        if df_daily_data.empty:
            e_msg = "No data could be retrieved from DB. Aborting."
            logger.error(e_msg)
            return {"Error": e_msg}, 500
        # Run SIRU model
        logger.debug("Initializing model...")
        model = ModelSIRU(days_infect_symp, days_infect_asymp)
        df_all_results = DataFrame(_create_empty_result_dict())
        for target_id in target_ids:
            logger.debug("Running model for target " + str(target_id))
            df_target_daily_data = \
                df_daily_data[df_daily_data["TARGET_ID"] == target_id][["REPORT_DATE", "CASE_COUNT",
                                                                        "CASE_SUM", "ICR_EXP_VAL"]]
            if df_target_daily_data.empty:
                logger.error("No data was found for target id " + str(target_id) + ". Moving on to next target id.")
                continue
            if target_id in population_dict.keys():
                target_population = population_dict[target_id]["POPULATION"]
            else:
                raise KeyError("No population given for id " + str(target_id))
            df_target_result = model.evaluate(df_target_daily_data, target_population)
            assert all([col in df_target_result.columns for col in MODEL_RESULT_FIELDS]), \
                "Invalid field returned by SIRU model: " \
                + ", ".join([col for col in MODEL_RESULT_FIELDS if col not in df_target_result.columns])
            df_result = df_target_result.rename(columns={
                ModelSIRU.COL_DATE: "DATE",
                ModelSIRU.COL_ACTIVE_REPORTED: "ACTIVE_REPORTED",
                ModelSIRU.COL_NEW_INFECTED: "NEW_INFECTIONS"
            })
            df_result["SUM_REPORTED"] = df_result["NEW_REPORTED"].cumsum()
            df_result["TARGET_ID"] = target_id
            df_result["ACTIVE_INFECTIONS"] = df_result["ACTIVE_REPORTED"] \
                                             + df_result[ModelSIRU.COL_ACTIVE_INFECTIONS_ASYMPTOMATIC] \
                                             + df_result[ModelSIRU.COL_ACTIVE_UNREPORTED]
            df_result["SUM_INFECTIONS"] = df_result["NEW_INFECTIONS"].cumsum()
            df_result = df_result.astype({
                "NEW_REPORTED": int,
                "ACTIVE_REPORTED": int,
                "ACTIVE_INFECTIONS": int,
                "NEW_INFECTIONS": int,
                "SUM_INFECTIONS": int,
                "TARGET_ID": int
            })
            assert all([col in df_result.columns for col in df_all_results.columns]), "Invalid columns in " + str(
                df_result.columns)
            df_all_results = df_all_results.append(df_result[list(df_all_results.columns)])
            # todo: compute ci bounds
        logger.info("Completed SIRU model running for all targets")
        # validate model return
        assert isinstance(df_all_results, DataFrame)
        assert set(df_all_results.columns) == RUNNER_RETURN_FIELDS, \
            "Invalid model result. Expected columns " + str(RUNNER_RETURN_FIELDS) \
            + ", but got " + str(df_all_results.columns)
        # return DataFrame
        return df_all_results.to_json(orient='table'), 200
    except Exception as e:
        logger.exception(e)
        return {str(e)}, 500
