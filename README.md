# dzr_model_sir

Implementations of [SIR](https://en.wikipedia.org/wiki/Compartmental_models_in_epidemiology#The_SIR_model) - based 
epidemiological models.

The models (currently: [SIRU](model/model_siru.py) are exposed to the outside world via a REST API, which allows to run a model via 
a _run_ endpoint. To separate the core model logic from request and data handling concerns,
handler modules (currently: [SIRURunner](model/siru_runner.py)) accept and validate requests, take care
of procuring the necessary data (note: this project requires read access to the DunkelzifferRadar database),
triggering the model run and compiling the API response object.

The project also contains deployment-related artifacts (Dockerfile, helm, Gitlab CI).


## Models

### SIRU
This [class](model/model_siru.py) represents our implementation of the model described in the following paper:

_Liu, Zhihua, and Glenn F. Webb. "Predicting the number of reported and unreported cases for the COVID-19    epidemics in China, South Korea, Italy, France, Germany and United Kingdom." medRxiv (2020)._

The acronym describes the four states that a population moves through during an epidemic:

**S**usceptibles

**I**nfectious

**R**eported

**U**nreported

Our approach differs from the original paper in several aspects:
* Modeling of time until today only: no prediction of the future is done. This relieves us of having to fit a function 
for extrapolation into the future. In the paper, the parameter tau (reproduction coefficient) is modeled this way.
* Time series of reported new cases as basis of computation: instead of estimating parameter tau, we compute active 
cases (R) based on the reported cases. By rearranging the formulae for states S, I, R and U from the paper by Liu et al., 
we are able to compute I from R and U from I subsequently. To account for seasonal (esp.: weekly) effects and missing 
data, we apply exponential smoothing, resampling and linear interpolation to the values for new reported cases. 
* Use of Infection-to-Case ratio (ICR) for parameter _f_: our implementation of the SIRU model relies on the 
availability of ICR values for the geography and timeframe to be modelled (there is in fact 
[another project](https://gitlab.gwdg.de/dunkelzifferradar/modelling/dzr_model_icr) in the workspace for ICR 
calculation). We use this ratio in SIRU to compute, for each day, how many infectious become _reported_ and how 
many become _unreported_ cases. Unlike the constant _f_ in the original paper, we use ICR values computed per day 
effectively as a function _f(t)_. 

Read more about our model [here](https://covid19.dunkelzifferradar.de/blog/model-siru).

## Try it out
### Setup
The following environment variables must be set to allow the code to connect to the database.
While you can set them in any way you want, e.g. as environment variables in your IDE or OS, the code uses
dotenv and if a file named ".env" in the project's root directory is present, it will be read and its values will
be used as environment variables.

Note that the code uses a library for PostgreSQL.

Required variables with sample values:
* DB_USER=postgres
* DB_PASSWORD=password
* DB_HOST=localhost
* DB_PORT=5432
* DB_NAME=dunkelzifferradar
* DB_SSL_CERT (can be empty)
* DB_SSL_KEY (can be empty)

To run a model locally via the REST API, the project needs to be run as a web service. To do this,
either run the [app.py](app.py), or build the project's [Dockerfile](Dockerfile). 
When running as a docker, do not forget to forward the API port specified in [app.py](app.py).

### Run
A model run can be triggered either via the respective REST API endpoint or in-code as demonstrated in 
[this experimentation code](experiments/eval_siru.py).

To run a model via the REST API, identify the endpoint and type (POST/GET) in [app.yml](app.yml) for the 
desired model. When running locally, the currently implemented SIRU model can be called at http://localhost:8078/api/model/siru/evaluate as 
a POST request. For easier experimentation, you can use the swagger UI in your browser 
at http://localhost:8078/api/model/siru/ui. 
