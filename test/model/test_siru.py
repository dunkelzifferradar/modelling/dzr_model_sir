import unittest
from datetime import datetime

from dotenv import load_dotenv
from pandas import read_csv, DataFrame
from pandas._testing import assert_frame_equal, assert_is_sorted

from consts import ROOT_DIR
from model.model_siru import ModelSIRU

TEST_POPULATION = 89504

load_dotenv(ROOT_DIR + "/.env")

TEST_DAYS_INFECT_ASYMP = 5
TEST_DAYS_INFECT_SYMP = 5


def to_date(date_string):
    return datetime.strptime(date_string, "%Y-%m-%d")


class TestModelSIRU(unittest.TestCase):

    def setUp(self):
        self.cut = ModelSIRU(TEST_DAYS_INFECT_SYMP, TEST_DAYS_INFECT_ASYMP)

    def test_evaluate(self):
        df_ts = read_csv(ROOT_DIR + "/test/data/input_timeseries.csv", parse_dates=["REPORT_DATE"])
        df_res: DataFrame = self.cut.evaluate(df_ts, TEST_POPULATION)
        # check that the expected dates are in the output
        exp_dates = set(df_ts["REPORT_DATE"].to_list())
        act_dates = set(df_res["REPORT_DATE"].to_list())
        self.assertEqual(exp_dates, act_dates)
        # check that df is sorted by date
        assert_is_sorted(df_res["REPORT_DATE"])
        # check output has expected columns
        exp_cols = {"REPORT_DATE", "REPORTED_ACTIVE_CASES", "NEW_REPORTED", "NEW_REMOVED_R", "NEW_INFECTED",
                    "ACTIVE_INFECTIONS_ASYMPTOMATIC", "UNREPORTED_ACTIVE_CASES", 'ICR_SMOOTHED'}
        act_cols = set(df_res.columns)
        self.assertEqual(exp_cols, act_cols)
        # check that numeric values are valid
        df_numeric = df_res[
            ["REPORTED_ACTIVE_CASES", "NEW_REPORTED", "NEW_REMOVED_R", "NEW_INFECTED", "ACTIVE_INFECTIONS_ASYMPTOMATIC",
             "UNREPORTED_ACTIVE_CASES"]]
        self.assertFalse(df_numeric.isnull().values.any())
        assert_frame_equal(df_numeric, df_numeric[(df_numeric >= 0).all(axis=1)])

    def tearDown(self):
        pass


if __name__ == '__main__':
    unittest.main()
