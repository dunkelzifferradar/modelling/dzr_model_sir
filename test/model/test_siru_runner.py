import json
import unittest
from datetime import datetime, timedelta
from unittest.mock import patch

from dotenv import load_dotenv
from dzr_shared_util.db.db_conn import DBConnector
from pandas import read_csv, read_json, DataFrame, Series
from pandas._testing import assert_is_sorted

from consts import ROOT_DIR
from model import siru_runner as cut
from model.model_siru import ModelSIRU

TEST_POPULATION = 89504

load_dotenv(ROOT_DIR + "/.env")

TEST_DAYS_INFECT_ASYMP = 5
TEST_DAYS_INFECT_SYMP = 5

mock_evaluate_counter = 0


def to_date(date_string):
    return datetime.strptime(date_string, "%Y-%m-%d")


def mock_get_data(tbl_name, where=""):
    where = where.replace("=", "==").replace(">==", ">=").replace("<==", "<=").replace("(", "[").replace(")", "]")
    if tbl_name.startswith("DM_MDL_PRM_"):
        date_col = ["REPORT_DATE"]
    elif tbl_name.startswith("SOR_MDL_ICR_"):
        date_col = ["DATE"]
    else:
        date_col = []
    samples_df = read_csv(ROOT_DIR + '/test/data/' + tbl_name + '.csv', sep=',', parse_dates=date_col)
    if where is not None and where != "":
        df_where_res = samples_df.query(where)
    else:
        df_where_res = samples_df
    return df_where_res


def mock_get_batch_data_basis(db, target_ids, target_level, icr_params):
    if target_level == 'COUNTY' and target_ids == [55, 56]:
        return DataFrame({"TARGET_ID": [55, 55, 56, 56, 56],
                          "REPORT_DATE": [to_date(dt) for dt in ["2020-08-03", "2020-08-05", "2020-08-03",
                                                                 "2020-08-06", "2020-08-07"]],
                          "CASE_COUNT": [0, 1, 0, 4, 1],
                          "CASE_SUM": [0, 1, 0, 4, 5],
                          "ICR_EXP_VAL": [0.1, 0.1, 0.05, 0.05, 0.05]
                          }), {55: {"POPULATION": 82000},
                               56: {"POPULATION": 7500}
                               }


def mock_evaluate(df_timeseries: DataFrame, population: int):
    global mock_evaluate_counter
    res = None
    if mock_evaluate_counter == 0:
        res = DataFrame({
            ModelSIRU.COL_DATE: [to_date(dt) for dt in ["2020-08-03", "2020-08-04", "2020-08-05"]],
            ModelSIRU.COL_NEW_REPORTED: [0, 0, 1],
            ModelSIRU.COL_ACTIVE_REPORTED: [0, 0, 1],
            ModelSIRU.COL_NEW_INFECTED: [4, 8, 10],
            ModelSIRU.COL_ACTIVE_INFECTIONS_ASYMPTOMATIC: [4, 12, 22],
            ModelSIRU.COL_ACTIVE_UNREPORTED: [3, 8, 9]
        })
    elif mock_evaluate_counter == 1:
        res = DataFrame({
            ModelSIRU.COL_DATE: [to_date(dt) for dt in
                                 ["2020-08-03", "2020-08-04", "2020-08-05", "2020-08-06", "2020-08-07"]],
            ModelSIRU.COL_NEW_REPORTED: [0, 0, 1, 2, 1],
            ModelSIRU.COL_ACTIVE_REPORTED: [0, 0, 1, 3, 2],
            ModelSIRU.COL_NEW_INFECTED: [4, 8, 10, 3, 5],
            ModelSIRU.COL_ACTIVE_INFECTIONS_ASYMPTOMATIC: [4, 12, 22, 18, 14],
            ModelSIRU.COL_ACTIVE_UNREPORTED: [3, 8, 9, 9, 4]
        })
    mock_evaluate_counter += 1
    return res


class TestModelSIRU(unittest.TestCase):
    ICR_RESULTS_TABLES = {
        "COUNTY": "SOR_MDL_ICR_COUNTY",
        "STATE": "SOR_MDL_ICR_STATE",
        "COUNTRY": "SOR_MDL_ICR_COUNTRY"
    }
    CASE_DATA_VIEWS = {
        "COUNTY": "DM_MDL_PRM_COUNTY",
        "STATE": "DM_MDL_PRM_STATE",
        "COUNTRY": "DM_MDL_PRM_COUNTRY"
    }

    def setUp(self):
        # self.db = DBConnector()
        pass

    @patch('dzr_shared_util.db.db_conn.DBConnector.__init__', return_value=None)
    @patch('dzr_shared_util.db.db_conn.DBConnector.get_data', side_effect=mock_get_data)
    def test_get_batch_data_basis(self, mock_init, mock_get_data_method):
        # Mocking the get_data method gets the ICR, case and population data for counties.
        #  In the data files that the mocked method loads from, there are three counties (1001, 1002, 1003)
        icr_params = {
            "IFR_BASE": 0.00358,
            "GEO_AGGR": True,
            "DAYS_AGGR": 7,
        }
        county_ids = [1001, 1002]
        self._test_get_batch_data_basis_for_target("COUNTY", county_ids, icr_params)
        state_ids = [14, 15]
        self._test_get_batch_data_basis_for_target("STATE", state_ids, icr_params)
        country_ids = [0]
        self._test_get_batch_data_basis_for_target("COUNTRY", country_ids, icr_params)

    def _test_get_batch_data_basis_for_target(self, level, target_ids, icr_params):
        self.db = DBConnector()
        df_daily_data, population_dict = cut._get_batch_data_basis(self.db, target_ids, level, icr_params)
        # assert that only the target ids given as input are present in the output
        self.assertEqual(set(target_ids), set(df_daily_data["TARGET_ID"].to_list()))
        self.assertEqual(set(target_ids), set(population_dict.keys()))
        # assert that there is an entry for each day in the given timeframe (defined by input data)
        #  the icr data is taken as reference here as in the icr service, this data is already filled to contain
        #  no skipped days
        df_icr_input = mock_get_data(self.ICR_RESULTS_TABLES[level],
                                     where="TARGET_ID in (" + ", ".join([str(x) for x in target_ids])
                                           + ") and IFR_BASE = " + str(icr_params["IFR_BASE"])
                                           + " and DAYS_AGGR = " + str(icr_params["DAYS_AGGR"])
                                           + " and GEO_AGGR = " + str(icr_params["GEO_AGGR"]))
        exp_days = df_icr_input["DATE"].tolist()
        act_days = df_daily_data["REPORT_DATE"].tolist()
        # make sure same number of entries
        self.assertEqual(len(exp_days), len(act_days))
        # make sure actual days are the same
        self.assertEqual(set(exp_days), set(act_days))
        # make sure output is sorted
        # check columns
        exp_cols = {"TARGET_ID", "REPORT_DATE", "CASE_COUNT", "CASE_SUM", "ICR_EXP_VAL"}
        self.assertEqual(exp_cols, set(df_daily_data.columns))
        df_case_input = mock_get_data(self.CASE_DATA_VIEWS[level])
        df_case_input = df_case_input[df_case_input["REPORT_DATE"].isin(df_daily_data["REPORT_DATE"])]
        for target_id in target_ids:
            df_filtered_on_target = df_daily_data[df_daily_data["TARGET_ID"] == target_id]
            df_case_input_for_target = df_case_input[df_case_input["TARGET_ID"] == target_id].copy().reset_index()
            # make sure it is sorted by date
            assert_is_sorted(df_filtered_on_target["REPORT_DATE"])
            # make sure sum of cases remained the same through transformations (for timeframe that input file specified)
            expected_target_sum = df_case_input_for_target["CASE_COUNT"].sum()
            df_filtered_on_target.set_index("REPORT_DATE", inplace=True)
            df_case_input_for_target.set_index("REPORT_DATE", inplace=True)
            final_day_of_expected_vals = max(
                df_case_input_for_target.index)
            actual_target_sum = df_filtered_on_target["CASE_COUNT"][
                                :final_day_of_expected_vals].sum()
            self.assertEqual(expected_target_sum, actual_target_sum)
            # make sure that filling of case date of final days for which no case data was delivered is correct
            unique_vals_after_last_input_case_count = \
                df_filtered_on_target[final_day_of_expected_vals + timedelta(days=1):]['CASE_COUNT'].unique()
            self.assertEqual(1, len(unique_vals_after_last_input_case_count))
            avg_last_week = round((expected_target_sum
                                 - sum(df_case_input_for_target['CASE_COUNT']
                                       [:final_day_of_expected_vals - timedelta(days=7)]))
                                / 7.0)
            self.assertEqual(avg_last_week, unique_vals_after_last_input_case_count[0])
            # make sure case_sum is monotonously increasing
            case_sums = df_filtered_on_target["CASE_SUM"].to_list()
            self.assertTrue([case_sums[i + 1] >= case_sums[i] for i in range(0, len(case_sums) - 1)])

    @patch('model.siru_runner._get_batch_data_basis', side_effect=mock_get_batch_data_basis)
    @patch('model.model_siru.ModelSIRU.__init__', return_value=None)
    @patch('model.model_siru.ModelSIRU.evaluate', side_effect=mock_evaluate)
    @patch('dzr_shared_util.db.db_conn.DBConnector.__init__', return_value=None)
    @patch('dzr_shared_util.db.db_conn.DBConnector.close_connection', return_value=None)
    def test_run_county(self, mock_get_batch_data_basis_method, mock_init, mock_evaluate_method,
                        mock_db_constructor, mock_close_connection):
        with open(ROOT_DIR + "/test/data/sample_api_input_county.json", 'r') as f:
            sample_input_dict = json.load(f)
        output_json_df, code = cut.run(sample_input_dict)
        df_output: DataFrame = read_json(output_json_df, orient="table")
        # check columns
        exp_cols = {"DATE",
                    "TARGET_ID",
                    "NEW_REPORTED",
                    "ACTIVE_REPORTED",
                    "SUM_REPORTED",
                    "NEW_INFECTIONS",
                    "ACTIVE_INFECTIONS",
                    "SUM_INFECTIONS"}
        self.assertTrue(all([field in df_output.columns for field in exp_cols]))
        # check target ids: same in in- and output
        self.assertEqual(sample_input_dict["TARGET_ID"], df_output["TARGET_ID"].unique().tolist())
        # check nas
        self.assertFalse(df_output.isnull().values.any())
        # check that model evaluation was called once for each target id in the input
        global mock_evaluate_counter
        self.assertEqual(len(Series(sample_input_dict["TARGET_ID"]).unique()), mock_evaluate_counter)

    def tearDown(self):
        # self.db.close_connection()
        pass


if __name__ == '__main__':
    unittest.main()
